import React from "react";
import ReactDOM from "react-dom";
import "./style.css";

function Zodiacs() {
  return (
    <div className="one">
      <h1>Знаки Зодіаку</h1>
      <table>
        <thead>
          <tr>
            <th>№</th>
            <th>Зодіак</th>
            <th>Дати</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>1</td>
            <td>Овен</td>
            <td>21 березня - 20 квітня</td>
          </tr>
          <tr>
            <td>2</td>
            <td>Телець</td>
            <td>21 квітня - 21 травня</td>
          </tr>
          <tr>
            <td>3</td>
            <td>Близнюки</td>
            <td>22 травня - 21 червня</td>
          </tr>
          <tr>
            <td>4</td>
            <td>Рак</td>
            <td>22 червня - 22 липня</td>
          </tr>
          <tr>
            <td>5</td>
            <td>Лев</td>
            <td>23 липня - 23 серпня</td>
          </tr>
          <tr>
            <td>6</td>
            <td>Діва</td>
            <td>24 серпня - 22 вересня</td>
          </tr>
          <tr>
            <td>7</td>
            <td>Терези</td>
            <td>23 вересня - 23 жовтня</td>
          </tr>
          <tr>
            <td>8</td>
            <td>Скорпіон</td>
            <td>24 жовтня - 22 листопада</td>
          </tr>
          <tr>
            <td>9</td>
            <td>Стрілець</td>
            <td>23 листопада - 21 грудня</td>
          </tr>
          <tr>
            <td>10</td>
            <td>Козоріг</td>
            <td>22 грудня - 20 січня</td>
          </tr>
          <tr>
            <td>11</td>
            <td>Водолій</td>
            <td>21 січня - 20 лютого</td>
          </tr>
          <tr>
            <td>12</td>
            <td>Риби</td>
            <td>21 лютого - 20 березня</td>
          </tr>
        </tbody>
      </table>
    </div>
  );
}

ReactDOM.render(<Zodiacs></Zodiacs>, document.querySelector("#root"));
